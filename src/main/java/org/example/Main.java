package org.example;

public class Main {
    public static void main(String[] args) {
        DataService dataService = new MongoDbDataService();
        var busCalService = new BusinessCalculationService(dataService);
        int maxNo = busCalService.findMax();
        System.out.println("Max number = " + maxNo);
    }
}