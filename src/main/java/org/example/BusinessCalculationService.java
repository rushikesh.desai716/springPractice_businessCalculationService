package org.example;

import jakarta.inject.Inject;
import org.springframework.stereotype.Service;

import java.util.Arrays;

//import jakarta.inject.Named;

//@Component
//@Named
@Service
public class BusinessCalculationService {

    private DataService dataService;

//    @Autowired  //  Constructor Injection
    @Inject
    public BusinessCalculationService(DataService dataService) {
        this.dataService = dataService;
    }

    public int findMax() {
        return Arrays.stream(dataService.retrieveData()).max().orElse(0);
    }

    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }
}
